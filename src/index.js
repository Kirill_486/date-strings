const moment = require('moment');
const numberOfCalls = 10E5;

const testMethodSync = (lambda) => {
    const sart = new Date().valueOf();
    for (let i=0; i< numberOfCalls; i++) {
        lambda();        
    }
    const end = new Date().valueOf();

    console.log(`${lambda.name} took ${end - sart}ms`);
}

const nativeDateMethod = () => {
    const date = new Date;
    const formatted = date.toLocaleString();
    return formatted;
}

const momentFormatDateMethod = () => {
    const date = moment();
    const formatted = date.format('D/MMM/YYYY  W H:m:ss');
    return formatted;
}

console.log(nativeDateMethod());
console.log('-----------------');

console.log(momentFormatDateMethod());
console.log('-----------------');


testMethodSync(nativeDateMethod);
testMethodSync(momentFormatDateMethod);

